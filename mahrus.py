print ("Program Pencarian")
print ("1.Pencarian Skuensial")
print ("2.Pencarian Biner")

inpt = input('Pilih Menu Anda : ')

if (inpt=="1" or inpt=="satu"):
    import json
    dirjson = open("mahrus.json")
    filejson = json.loads(dirjson.read())
    skuensial = filejson['skuensial']

    find = int(input("Cari Angka : "))
    if find not in skuensial:
        print("Angka Tidak Ditemukan")
    else:
        print("Angka", find, "Ditemukan")
elif (inpt=="2" or inpt=="dua"):
    import json
    dirjson = open("mahrus.json")
    filejson = json.loads(dirjson.read())
    skuensial = filejson['biner']

    find = int(input("Cari Angka : "))
    if find not in skuensial:
        print("Angka Tidak Ditemukan")
    else:
        print("Angka", find, "Ditemukan")
else:
    print("Menu Tidak Ada")
